<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Final Project

<p>
<b>Kelompok 6</b>
</p>

<p>
<b>Anggota Kelompok :</b></p>
<p>1. <a href="https://gitlab.com/Rudiyansyah/finalproject/-/tree/main">Rudiyansyah</a></p>
<p>2. <a href="https://gitlab.com/Rudiyansyah/finalproject/-/tree/guestamukti">Guesta Mukti</a></p>
<p>3. <a href="https://gitlab.com/Rudiyansyah/finalproject/-/tree/ameliacha">Amelia Chairunnisa</a></p>
</p>
<p>
<b>Tema Project</b>
Forum Tanya Jawab Bisa dari Frontend dan Backend
</p>

<p><strong>ERD</strong></p>

<img src="https://gitlab.com/Rudiyansyah/finalproject/-/raw/main/public/image/ERD_finalproject.png">




