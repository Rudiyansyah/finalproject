<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class JawabanController extends Controller
{
    public function create()
    {
        $cast2 = DB::table('kategori')->get();
        $cast3 = DB::table('pertanyaan')->get();
 //dd($cast2);
        return view('jawaban.create', compact('cast2','cast3'));
        //return view('jawaban.create');
    }

    public function index()
    {
        //$cast = DB::table('cast')->all();
 
        //return view('post.index', compact('cast'));
        //$cast2 = DB::table('jawaban')->get();
 //dd($cast2);
        //return view('jawaban.index', compact('cast2'));
        //return view('post.index');
        $cast2 = DB::table('jawaban')
        ->select('jawaban.id as idj', 'jawaban.jawaban','jawaban.user_id','jawaban.pertanyaan_id','pertanyaan.tulisan','pertanyaan.kategori_id','pertanyaan.gambar')
        ->join('pertanyaan', 'jawaban.pertanyaan_id', '=', 'pertanyaan.id')
        ->where('jawaban.user_id', '=', 1) //nanti user id ini ganti Auth::user()->id;
        ->get();
        return view('jawaban.index', compact('cast2'));
//tampilkan view barang dan kirim datanya ke view tersebut
//return view('barang')->with('data', $data);


    }

    public function store(Request $request)
    {
        //dd($request->all());
        /*$request->validate([
            'nama' => 'required|max:25',
            'bio' => 'required',
        ]);*/

        DB::table('jawaban')->insert(
    [
        'jawaban' => $request['jawaban'],
        
        'pertanyaan_id' => $request['tulisan'],
        'user_id' => 1 //ganti dgn auth::user
        
    ]
    
);
return redirect('/jawaban');
    }
    
    public function show($id)
    {
        $cast2 = DB::table('jawaban')
        ->join('pertanyaan', 'jawaban.pertanyaan_id', '=', 'pertanyaan.id')
        ->where('jawaban.id', $id)
        ->first(); 
       // dd($cast2);
        //->get();
      //  return view('jawaban.index', compact('cast2'));

    //$cast2 = DB::table('jawaban')->where('id', $id)->first();//ambil 1 data saja
    return view('jawaban.detail',compact('cast2'));
    }

    public function edit($id)
    {
        $cast2 = DB::table('jawaban')
        ->select('jawaban.id as idj', 'jawaban.jawaban','jawaban.user_id','jawaban.pertanyaan_id','pertanyaan.tulisan','pertanyaan.kategori_id','pertanyaan.gambar')
        ->join('pertanyaan', 'jawaban.pertanyaan_id', '=', 'pertanyaan.id')
        ->where('jawaban.id', $id)
        ->first(); 
        //$cast3 = DB::table('kategori')->get();
    //$cast2 = DB::table('jawaban')->where('id', $id)->first();//ambil 1 data saja
    return view('jawaban.edit',compact('cast2'));
    }


    public function update(Request $request,$id)
    {
        //dd($request->all());
        /*$request->validate([
            'tulisan' => 'required|max:100',
            
        ]);*/

        

    $update = DB::table('jawaban')
              ->where('id', $id)
              ->update(
                [
                    'jawaban' => $request['jawaban']
                    
                ]
            );
            //dd($update);
           return redirect('/jawaban'); 
    }

    public function destroy(Request $request,$id)
    {
        DB::table('jawaban')->where('id', $id)->delete();
        return redirect('/jawaban'); 
    }
    
}
