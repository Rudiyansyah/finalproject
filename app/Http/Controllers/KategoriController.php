<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function create()
    {
        
        return view('kategori.create');
    }

    public function index()
    {
        //$cast = DB::table('cast')->all();
        //dd($cast2);
        //return view('post.index', compact('cast'));
        $cast2 = DB::table('kategori')->get();
 
        return view('kategori.index', compact('cast2'));
        //return view('post.index');

    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required',
            
        ]);
        DB::table('kategori')->insert(
            [
                'nama_kategori' => $request['nama'],
                'user_id' => 2
            ]
            
        );
        return redirect('/kategori');
    }
    
    public function show($id)
    {

    $cast2 = DB::table('kategori')->where('id', $id)->first();//ambil 1 data saja
    return view('kategori.detail',compact('cast2'));
    }

    public function edit($id)
    {

    $cast2 = DB::table('kategori')->where('id', $id)->first();//ambil 1 data saja
    return view('kategori.edit',compact('cast2'));
    }


    public function update(Request $request,$id)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required|max:25',
            
        ]);

        

    DB::table('kategori')
              ->where('id', $id)
              ->update(
                [
                    'nama_kategori' => $request['nama'],
                    
                ]
            );
           return redirect('/kategori'); 
    }

    public function destroy(Request $request,$id)
    {
        DB::table('kategori')->where('id', $id)->delete();
        return redirect('/kategori'); 
    }
    
}
