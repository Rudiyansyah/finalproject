<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function create()
    {
        $cast2 = DB::table('kategori')->get();
 //dd($cast2);
        return view('pertanyaan.create', compact('cast2'));
        //return view('pertanyaan.create');
    }

    public function index()
    {
        //$cast = DB::table('cast')->all();
 
        //return view('post.index', compact('cast'));
        //$cast2 = DB::table('pertanyaan')->get();
 //dd($cast2);
        //return view('pertanyaan.index', compact('cast2'));
        //return view('post.index');
        $cast2 = DB::table('pertanyaan')
        ->select('pertanyaan.id as idp', 'pertanyaan.tulisan','kategori.nama_kategori')
        ->join('kategori', 'pertanyaan.kategori_id', '=', 'kategori.id')
        ->where('pertanyaan.user_id', '=', 1) //nanti user id ini ganti Auth::user()->id;
        ->get();
        return view('pertanyaan.index', compact('cast2'));
//tampilkan view barang dan kirim datanya ke view tersebut
//return view('barang')->with('data', $data);


    }

    public function store(Request $request)
    {
        //dd($request->all());
        /*$request->validate([
            'nama' => 'required|max:25',
            'bio' => 'required',
        ]);*/

        $resorce       = $request->file('gambar');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/image", $name);
            

        DB::table('pertanyaan')->insert(
    [
        'tulisan' => $request['tulisan'],
        'gambar' => $name,
        'kategori_id' => $request['kategori'],
        'user_id' => 1
        
    ]
    
);
return redirect('/pertanyaan');
    }
    
    public function show($id)
    {
        $cast2 = DB::table('pertanyaan')
        ->join('kategori', 'pertanyaan.kategori_id', '=', 'kategori.id')
        ->where('pertanyaan.id', $id)
        ->first(); 
        //dd($cast2);
        //->get();
      //  return view('pertanyaan.index', compact('cast2'));

    //$cast2 = DB::table('pertanyaan')->where('id', $id)->first();//ambil 1 data saja
    return view('pertanyaan.detail',compact('cast2'));
    }

    public function edit($id)
    {
        $cast2 = DB::table('pertanyaan')
        ->join('kategori', 'pertanyaan.kategori_id', '=', 'kategori.id')
        ->where('pertanyaan.id', $id)
        ->first(); 
        $cast3 = DB::table('kategori')->get();
    //$cast2 = DB::table('pertanyaan')->where('id', $id)->first();//ambil 1 data saja
    return view('pertanyaan.edit',compact('cast2','cast3'));
    }


    public function update(Request $request,$id)
    {
        //dd($request->all());
        /*$request->validate([
            'tulisan' => 'required|max:100',
            
        ]);*/
        $resorce       = $request->file('gambar');
        $name   = $resorce->getClientOriginalName();
        $resorce->move(\base_path() ."/public/image", $name);
        

    $update = DB::table('pertanyaan')
              ->where('id', $id)
              ->update(
                [
                    'tulisan' => $request['tulisan'],
                    'kategori_id' => $request['kategori'],
                    'gambar' => $name
                ]

            );
            //dd($update);
           return redirect('/pertanyaan'); 
    }

    public function destroy(Request $request,$id)
    {
        DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan'); 
    }
    
}
