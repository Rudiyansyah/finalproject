<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function create()
    {
        return view('post.create');
    }

    public function index()
    {
        //$cast = DB::table('cast')->all();
 
        //return view('post.index', compact('cast'));
        $cast2 = DB::table('tb_post')->get();
 //dd($cast2);
        return view('post.index', compact('cast2'));
        //return view('post.index');

    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required|max:25',
            'bio' => 'required',
        ]);

        DB::table('tb_post')->insert(
    [
        'title' => $request['nama'],
        'body' => $request['bio']
    ]
    
);
return redirect('/post');
    }
    
    public function show($id)
    {

    $cast2 = DB::table('tb_post')->where('id', $id)->first();//ambil 1 data saja
    return view('post.detail',compact('cast2'));
    }

    public function edit($id)
    {

    $cast2 = DB::table('tb_post')->where('id', $id)->first();//ambil 1 data saja
    return view('post.edit',compact('cast2'));
    }


    public function update(Request $request,$id)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required|max:25',
            'bio' => 'required',
        ]);

        

    DB::table('tb_post')
              ->where('id', $id)
              ->update(
                [
                    'title' => $request['nama'],
                    
                    'body' => $request['bio']
                ]
            );
           return redirect('/post'); 
    }

    public function destroy(Request $request,$id)
    {
        DB::table('tb_post')->where('id', $id)->delete();
        return redirect('/post'); 
    }
    
}
