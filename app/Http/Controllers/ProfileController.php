<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    
    public function index()
    {
        
        //$cast2 = DB::table('profile')->get();
        $cast2 = DB::table('user')->where('id', 2)->first();//ambil 1 data saja
        //$cast3 = DB::table('profile')->where('id',$cast2->id)->first();//ambil 1 data saja
        return view('profile.index', compact('cast2'));
        //return view('profile.index');

    }

        
    public function show($id)
    {

    $cast2 = DB::table('profile')->where('user_id', $id)->first();//ambil 1 data saja
    return view('profile.detail',compact('cast2'));
    }

    public function edit($id)
    {

    $cast2 = DB::table('profile')->where('user_id', $id)->first();//ambil 1 data saja
    return view('profile.edit',compact('cast2'));
    }


    public function update(Request $request,$id)
    {
        //dd($request->all());
       /* $request->validate([
            'nama' => 'required|max:25',
            
        ]);*/

        

    DB::table('profile')
              ->where('user_id', $id)
              ->update(
                [
                    'biodata' => $request['biodata'],
                    'umur' => $request['umur'],
                    'email' => $request['email'],
                    'alamat' => $request['alamat']
                    
                ]
            );
           return redirect('/profile'); 
    }

    
    
}
