@extends('layout.master')
@section('judul')
    
        Halaman Detail
        @endsection
        @section('content')
        
        <form action="/cast" method="post">
        @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" value="{{$cast2->nama}}" class="form-control">
  </div>
  
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" value="{{$cast2->umur}}"class="form-control">
  </div>
  
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" cols="30" class="form-control">{{$cast2->bio}}</textarea>
    
  </div>
  
</form>
        @endsection