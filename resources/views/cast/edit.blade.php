@extends('layout.master')
@section('judul')
    
        Halaman Edit
        @endsection
        @section('content')
        
        <form action="/cast/{{$cast2->id}}" method="post">
        @csrf
        @method('put')
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" value="{{$cast2->nama}}" class="form-control">
  </div>
  
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" value="{{$cast2->umur}}"class="form-control">
  </div>
  
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" cols="30" class="form-control">{{$cast2->bio}}</textarea>
    
  </div>
  <button type="submit" class="btn btn-warning">Update</button>
</form>
        @endsection