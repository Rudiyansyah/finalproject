@extends('layout.master')
@section('judul')
    
        Halaman List
        @endsection
        @section('content')
        <a href="/cast/create" class="btn btn-primary btn-sm">tambah cast</a>
        <table class="table table-borderless">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">title</th>
      <th scope="col">body</th>
      
      <th scope="col">aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse($cast2 as $key=>$item)
<tr>
<td>{{$key +1}}</td>
<td>{{$item->title}}</td>
<td>{{$item->body}}</td>

<td>
<form action="/post/{{$item->id}}" method="post">
@csrf
    @method('delete')
    <a href="/post/{{$item->id}}" class="btn btn-info btn-sm">detail</a> 
<a href="/post/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a> 
    <input type="submit" value="delete" class="btn btn-danger btn-sm">

</form>
</td>
</tr>
    @empty
<tr>
<td>kosong</td>
</tr>

@endforelse

    
    
    
  </tbody>
</table>

        @endsection