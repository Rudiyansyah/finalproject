@extends('layout.master')
@section('judul')
    
        Halaman Tambah Jawaban dari Dashboard 
        @endsection
        @section('content')

        <form action="/tambahjawaban" method="post">
        @csrf

  
  <div class="form-group">
  <label>Pertanyaan User Lain</label>
    <select name="tulisan" class="form-control">
    @forelse($cast3 as $key=>$item)
    <option value="{{$item->id}}">{{$item->tulisan}}</option>

    @empty
<option value="">pertanyaan kosong</option>
    @endforelse

</select>
  </div>

  <div class="form-group">
    <label>Jawaban Kamu</label>
    <input type="text" name="jawaban" class="form-control">
  </div>
  @error('Jawaban')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
    
