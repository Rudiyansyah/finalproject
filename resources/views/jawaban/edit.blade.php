@extends('layout.master')
@section('judul')
    
        Halaman Edit
        @endsection
        @section('content')
        
        <form action="/jawaban/{{$cast2->idj}}" method="post">
        @csrf
        @method('put')
  <div class="form-group">
    <label>Jawaban Kamu</label>
    <input type="text" name="jawaban" value="{{$cast2->jawaban}}" class="form-control">
  </div>

  <div class="form-group">
    <label>Pertanyaan User Lain</label>
    <input type="text" name="tulisan" value="{{$cast2->tulisan}}" class="form-control" disabled>
  </div>
  
  <button type="submit" class="btn btn-warning">Update</button>
</form>
        @endsection