@extends('layout.master')
@section('judul')
    
        Halaman List Jawaban
        @endsection
        @section('content')
        <a href="/jawaban/create" class="btn btn-primary btn-sm">Yuk Jawab Pertanyaan</a>
        
        <table class="table table-borderless">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Jawaban</th>
    <th>Pertanyaan</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse($cast2 as $key=>$item)
<tr>
<td>{{$key +1}}</td>
<td>{{$item->jawaban}}</td>
<td>{{$item->tulisan}}</td>

<td>
<form action="/jawaban/{{$item->idj}}" method="post">
@csrf
    @method('delete')
    <a href="/jawaban/{{$item->idj}}" class="btn btn-info btn-sm">detail</a> 
<a href="/jawaban/{{$item->idj}}/edit" class="btn btn-warning btn-sm">edit</a> 
    <input type="submit" value="delete" class="btn btn-danger btn-sm">

</form>
</td>
</tr>
    @empty
<tr>
<td>kosong</td>
</tr>

@endforelse

    
    
    
  </tbody>
</table>

        @endsection