@extends('layout.master')
@section('judul')
    
        Halaman Edit
        @endsection
        @section('content')
        
        <form action="/kategori/{{$cast2->id}}" method="post">
        @csrf
        @method('put')
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="text" name="nama" value="{{$cast2->nama_kategori}}" class="form-control">
  </div>
  
  <button type="submit" class="btn btn-warning">Update</button>
</form>
        @endsection