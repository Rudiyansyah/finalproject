@extends('layout.master')
@section('judul')
    
        Halaman List Kategori
        @endsection
        @section('content')
        <a href="/kategori/create" class="btn btn-primary btn-sm">tambah kategori</a>
        <table class="table table-borderless">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Kategori</th>
    
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse($cast2 as $key=>$item)
<tr>
<td>{{$key +1}}</td>
<td>{{$item->nama_kategori}}</td>


<td>
<form action="/kategori/{{$item->id}}" method="post">
@csrf
    @method('delete')
    <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">detail</a> 
<a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a> 
    <input type="submit" value="delete" class="btn btn-danger btn-sm">

</form>
</td>
</tr>
    @empty
<tr>
<td>kosong</td>
</tr>

@endforelse

    
    
    
  </tbody>
</table>

        @endsection