@extends('layout.master')
@section('judul')
    
        Halaman Tambah Pertanyaan
        @endsection
        @section('content')

        <form action="/tambahpertanyaan" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
    <label>Tulisan</label>
    <input type="text" name="tulisan" class="form-control">
  </div>
   <div class="form-group">
    <label>Kategori</label>
    <select name="kategori" class="form-control">
    @forelse($cast2 as $key=>$item)
    <option value="{{$item->id}}">{{$item->nama_kategori}}</option>

    @empty
<option value="">kategori kosong</option>
    @endforelse

</select>
  </div>

<div class="form-group">
    <label>Gambar</label>
    <input type="file" name="gambar" class="form-control">
</div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
    
