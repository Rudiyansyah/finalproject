@extends('layout.master')
@section('judul')
    
        Halaman Edit
        @endsection
        @section('content')

        <form action="/pertanyaan/{{$cast2->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
    <label>Tulisan</label>
    <input type="text" name="tulisan" value="{{$cast2->tulisan}}" class="form-control">
  </div>
  <div class="form-group">
    <label>Kategori</label>
    <select name="kategori" class="form-control">
    @forelse($cast3 as $key=>$item)
    @if($cast2->kategori_id == $item->id)

    <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>

    @else
    <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
    @endif

    @empty
<option value="">kategori kosong</option>
    @endforelse

</select>
  </div>
  
  

   
<div class="form-group">
    <label>Gambar</label>
    <input type="file" name="gambar" class="form-control">
</div>
<button type="submit" class="btn btn-warning">Update</button> 
</form>
        
        
        @endsection