@extends('layout.master')
@section('judul')
    
        Halaman List Pertanyaan
        @endsection
        @section('content')
        <a href="/pertanyaan/create" class="btn btn-primary btn-sm">tambah pertanyaan</a>
        <table class="table table-borderless">
  <thead>
    <tr>
      <th scope="col">#</th>
      
      <th scope="col">Pertanyaan</th>
      <th scope="col">Kategori</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    @forelse($cast2 as $key=>$item)
<tr>
<td>{{$key +1}}</td>
<td>{{$item->tulisan}}</td>
<td>{{$item->nama_kategori}}</td>

<td>
<form action="/pertanyaan/{{$item->idp}}" method="post">
@csrf
    @method('delete')
    <a href="/pertanyaan/{{$item->idp}}" class="btn btn-info btn-sm">detail</a> 
<a href="/pertanyaan/{{$item->idp}}/edit" class="btn btn-warning btn-sm">edit</a> 
    <input type="submit" value="delete" class="btn btn-danger btn-sm">

</form>
</td>
</tr>
    @empty
<tr>
<td>kosong</td>
</tr>

@endforelse

    
    
    
  </tbody>
</table>

        @endsection