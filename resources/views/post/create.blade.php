@extends('layout.master')
@section('judul')
    
        Halaman Tambah
        @endsection
        @section('content')

        <form action="/post" method="post">
        @csrf
  <div class="form-group">
    <label>Title</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  
  <div class="form-group">
    <label>Body</label>
    <textarea name="bio" cols="30" class="form-control"></textarea>
    
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
    
