@extends('layout.master')
@section('judul')
    
        Halaman Edit
        @endsection
        @section('content')
        
        <form action="/post/{{$cast2->id}}" method="post">
        @csrf
        @method('put')
  <div class="form-group">
    <label>title</label>
    <input type="text" name="nama" value="{{$cast2->title}}" class="form-control">
  </div>
  
  <div class="form-group">
    <label>body</label>
    <textarea name="bio" cols="30" class="form-control">{{$cast2->body}}</textarea>
    
  </div>
  <button type="submit" class="btn btn-warning">Update</button>
</form>
        @endsection