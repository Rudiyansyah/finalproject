@extends('layout.master')
@section('judul')
    
        Halaman Tambah Kategori
        @endsection
        @section('content')

        <form action="/tambahkategori" method="post">
        @csrf
  <div class="form-group">
    <label>Kategori</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('kategori')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
    
