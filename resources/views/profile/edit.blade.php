@extends('layout.master')
@section('judul')
    
        Halaman Edit
        @endsection
        @section('content')
        
        <form action="/profile/{{$cast2->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
    <label>Biodata</label>
    <textarea name="biodata" class="form-control">{{$cast2->biodata}}</textarea>
  </div>

  <div class="form-group">
    <label>Umur</label>
    <input type="number" name="umur" value="{{$cast2->umur}}" class="form-control">
  </div>

  <div class="form-group">
    <label>Email</label>
    <input type="email" name="email" value="{{$cast2->email}}" class="form-control">
  </div>
  
  <div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat" class="form-control">{{$cast2->alamat}}</textarea>
  </div>
  
  <button type="submit" class="btn btn-warning">Update</button>
</form>
        @endsection