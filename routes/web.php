<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/post/create', 'PostController@create');
Route::post('/post', 'PostController@store');
Route::get('/post', 'PostController@index');
Route::get('/post/{id}', 'PostController@show');
Route::get('/post/{id}/edit', 'PostController@edit');
Route::put('/post/{id}', 'PostController@update');
Route::delete('/post/{cast_id}', 'PostController@destroy');

Route::resource('/post','PostController');

Route::get('/kategori/create', 'KategoriController@create');
Route::post('/tambahkategori', 'KategoriController@store');
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{id}', 'KategoriController@show');
Route::get('/kategori/{id}/edit', 'KategoriController@edit');
Route::put('/kategori/{id}', 'KategoriController@update');
Route::delete('/kategori/{id}', 'KategoriController@destroy');

Route::resource('/kategori','KategoriController');

Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/tambahpertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::post('/pertanyaan/{id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');

Route::get('/jawaban/create', 'JawabanController@create');
Route::post('/tambahjawaban', 'JawabanController@store');
Route::get('/jawaban', 'JawabanController@index');
Route::get('/jawaban/{id}', 'JawabanController@show');
Route::get('/jawaban/{id}/edit', 'JawabanController@edit');
Route::put('/jawaban/{id}', 'JawabanController@update');
Route::delete('/jawaban/{id}', 'JawabanController@destroy');


Route::get('/profile', 'ProfileController@index');
Route::get('/profile/{id}', 'ProfileController@show');
Route::get('/profile/{id}/edit', 'ProfileController@edit');
Route::put('/profile/{id}', 'ProfileController@update');


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
